package ru.vsgtu.schedule.server.service;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vsgtu.schedule.server.domain.ExecutionLog;
import ru.vsgtu.schedule.server.domain.ExecutionResult;
import ru.vsgtu.schedule.server.repository.ExecutionLogRepository;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by HOME on 24.04.2016.
 */
@Service
@Transactional
public class ExecutionLogService {
    @Inject
    private ExecutionLogRepository executionLogRepository;

    public void success(LocalDateTime start) {
        ExecutionLog log = new ExecutionLog();
        log.setStartTime(start);
        log.setEndTime(LocalDateTime.now());
        log.setResult(ExecutionResult.SUCCESS);
        executionLogRepository.saveAndFlush(log);
    }

    public void error(LocalDateTime start, Exception e) {
        ExecutionLog log = new ExecutionLog();
        log.setStartTime(start);
        log.setEndTime(LocalDateTime.now());
        log.setResult(ExecutionResult.ERROR);
        log.setMessage(ExceptionUtils.getMessage(e));
        executionLogRepository.saveAndFlush(log);
    }
    @Transactional(readOnly = true)
    public List<ExecutionLog> getAll() {
        return executionLogRepository.findAll();
    }
}
