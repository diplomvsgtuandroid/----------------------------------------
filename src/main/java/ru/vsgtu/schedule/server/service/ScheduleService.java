package ru.vsgtu.schedule.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vsgtu.schedule.server.domain.Auditory;
import ru.vsgtu.schedule.server.domain.Group;
import ru.vsgtu.schedule.server.domain.Main;
import ru.vsgtu.schedule.server.domain.MainStatus;
import ru.vsgtu.schedule.server.domain.Subject;
import ru.vsgtu.schedule.server.domain.Teacher;
import ru.vsgtu.schedule.server.repository.AuditoryRepository;
import ru.vsgtu.schedule.server.repository.GroupRepository;
import ru.vsgtu.schedule.server.repository.MainRepository;
import ru.vsgtu.schedule.server.repository.SubjectRepository;
import ru.vsgtu.schedule.server.repository.TeacherRepository;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by HOME on 01.05.2016.
 */
@Service
@Transactional(readOnly = true)
public class ScheduleService {
    private final Logger log = LoggerFactory.getLogger(ScheduleService.class);
    @Inject
    private TeacherRepository teacherRepository;

    @Inject
    private SubjectRepository subjectRepository;

    @Inject
    private GroupRepository groupRepository;

    @Inject
    private AuditoryRepository auditoryRepository;

    @Inject
    private MainRepository mainRepository;

    public List<Teacher> getTeacherAll() {
        return teacherRepository.findAll();
    }

    public List<Subject> getSubjectAll() {
        return subjectRepository.findAll();
    }


    public List<Main> getMainAllSuccess() {
        return mainRepository.findByStatus(MainStatus.SUCCESS);
    }

    public List<Main> getMainAll() {
        return mainRepository.findAllOrderByGroupName();
    }

    public Main getMain(Long id) {
        return mainRepository.findOne(id);
    }

    public List<Group> getGroupAll() {
        return groupRepository.findAll();
    }

    public List<Auditory> getAuditoryAll() {
        return auditoryRepository.findAll();
    }

    @Transactional()
    public Group getByName(String name) {
        return groupRepository.findOneByName(name).orElseGet(() -> {
            log.info("Group not found create new group {}", name);
            return groupRepository.saveAndFlush(new Group(name));
        });
    }

    public Subject getSubjectById(Long id) {
        return subjectRepository.findById(id).orElseThrow(() -> new IllegalStateException("Subject not found " + id));
    }

    public Auditory getAuditoryById(Long id) {
        return auditoryRepository.findOneById(id).orElseThrow(() -> new IllegalStateException("Auditory not found " + id));
    }

    public Teacher getTeacherById(Long id) {
        return teacherRepository.findOneById(id).orElseThrow(() -> new IllegalStateException("Teacher not found " + id));
    }

    public Group getGroupById(Long id) {
        return Optional.ofNullable(groupRepository.findOne(id)).orElseThrow(() -> new IllegalStateException("Teacher not found " + id));
    }

    @Transactional()
    public void updateMainUpdate(List<Main> list) {
        mainRepository.deleteAllInBatch();
        mainRepository.save(list);
    }

    @Transactional
    public void updateGroup(Group group) {
        groupRepository.save(group);
    }
    @Transactional
    public void updateTeacher(Teacher teacher) {
        teacherRepository.save(teacher);
    }
}
