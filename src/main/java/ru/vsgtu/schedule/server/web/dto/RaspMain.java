
package ru.vsgtu.schedule.server.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "subject_id",
    "teacher_id",
    "auditory",
    "pair_type",
    "pair_number",
    "group_number",
    "day_of_week",
    "week"
})
public class RaspMain {

    @JsonProperty("subject_id")
    private String subjectId;
    @JsonProperty("teacher_id")
    private String teacherId;
    @JsonProperty("auditory")
    private String auditory;
    @JsonProperty("pair_type")
    private String pairType;
    @JsonProperty("pair_number")
    private String pairNumber;
    @JsonProperty("group_number")
    private String groupNumber;
    @JsonProperty("day_of_week")
    private String dayOfWeek;
    @JsonProperty("week")
    private String week;

    /**
     * 
     * @return
     *     The subjectId
     */
    @JsonProperty("subject_id")
    public String getSubjectId() {
        return subjectId;
    }

    /**
     * 
     * @param subjectId
     *     The subject_id
     */
    @JsonProperty("subject_id")
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * 
     * @return
     *     The teacherId
     */
    @JsonProperty("teacher_id")
    public String getTeacherId() {
        return teacherId;
    }

    /**
     * 
     * @param teacherId
     *     The teacher_id
     */
    @JsonProperty("teacher_id")
    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * 
     * @return
     *     The auditory
     */
    @JsonProperty("auditory")
    public String getAuditory() {
        return auditory;
    }

    /**
     * 
     * @param auditory
     *     The auditory
     */
    @JsonProperty("auditory")
    public void setAuditory(String auditory) {
        this.auditory = auditory;
    }

    /**
     * 
     * @return
     *     The pairType
     */
    @JsonProperty("pair_type")
    public String getPairType() {
        return pairType;
    }

    /**
     * 
     * @param pairType
     *     The pair_type
     */
    @JsonProperty("pair_type")
    public void setPairType(String pairType) {
        this.pairType = pairType;
    }

    /**
     * 
     * @return
     *     The pairNumber
     */
    @JsonProperty("pair_number")
    public String getPairNumber() {
        return pairNumber;
    }

    /**
     * 
     * @param pairNumber
     *     The pair_number
     */
    @JsonProperty("pair_number")
    public void setPairNumber(String pairNumber) {
        this.pairNumber = pairNumber;
    }

    /**
     * 
     * @return
     *     The groupNumber
     */
    @JsonProperty("group_number")
    public String getGroupNumber() {
        return groupNumber;
    }

    /**
     * 
     * @param groupNumber
     *     The group_number
     */
    @JsonProperty("group_number")
    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    /**
     * 
     * @return
     *     The dayOfWeek
     */
    @JsonProperty("day_of_week")
    public String getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * 
     * @param dayOfWeek
     *     The day_of_week
     */
    @JsonProperty("day_of_week")
    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * 
     * @return
     *     The week
     */
    @JsonProperty("week")
    public String getWeek() {
        return week;
    }

    /**
     * 
     * @param week
     *     The week
     */
    @JsonProperty("week")
    public void setWeek(String week) {
        this.week = week;
    }
}
