package ru.vsgtu.schedule.server.web.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.vsgtu.schedule.server.service.UpdateScheduleTask;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
@Named("admin")
@ViewScoped
public class AdminBean {
    private final Logger log = LoggerFactory.getLogger(AdminBean.class);
    @Inject
    private UpdateScheduleTask updateScheduleTask;

    public void updateSchedule() {
        try {
            updateScheduleTask.update();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Обновление успешно"));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Найдены ошибки"));
        }
    }
}
