package ru.vsgtu.schedule.server.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.vsgtu.schedule.server.domain.Group;
import ru.vsgtu.schedule.server.domain.Main;
import ru.vsgtu.schedule.server.domain.Subject;
import ru.vsgtu.schedule.server.domain.SubjectType;
import ru.vsgtu.schedule.server.domain.Teacher;
import ru.vsgtu.schedule.server.service.ScheduleService;
import ru.vsgtu.schedule.server.web.dto.RaspAuditory;
import ru.vsgtu.schedule.server.web.dto.RaspDmain;
import ru.vsgtu.schedule.server.web.dto.RaspGroup;
import ru.vsgtu.schedule.server.web.dto.RaspMain;
import ru.vsgtu.schedule.server.web.dto.RaspSubject;
import ru.vsgtu.schedule.server.web.dto.RaspTeachers;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by HOME on 24.04.2016.
 */
@RestController
@RequestMapping(value = "/content/rasp", produces = MediaType.APPLICATION_JSON_VALUE)
public class ScheduleResource {
    @Inject
    private ScheduleService scheduleService;

    @RequestMapping(value = "/rasp_dmain.json",
            method = RequestMethod.GET)
    @Timed
    public List<RaspDmain> getDmain() {
        return Arrays.asList(new RaspDmain(), new RaspDmain());
    }

    @RequestMapping(value = "/rasp_group.json",
            method = RequestMethod.GET)
    @Timed
    public List<RaspGroup> getRGroup() {
        return scheduleService.getGroupAll().stream().map(this::toGroup).collect(Collectors.toList());
    }

    @RequestMapping(value = "/rasp_auditory.json",
            method = RequestMethod.GET)
    @Timed
    public List<RaspAuditory> getAuditory() {
        return Arrays.asList(new RaspAuditory(), new RaspAuditory());
    }

    @RequestMapping(value = "/rasp_main.json",
            method = RequestMethod.GET)
    @Timed
    public List<RaspMain> getMain() {
        return scheduleService.getMainAllSuccess().stream().map(this::toMain).collect(Collectors.toList());
    }

    @RequestMapping(value = "/rasp_subjects.json",
            method = RequestMethod.GET)
    @Timed
    public List<RaspSubject> getSubjects() {
        return scheduleService.getSubjectAll().stream().map(this::toSubject).collect(Collectors.toList());
    }

    @RequestMapping(value = "/rasp_teachers.json",
            method = RequestMethod.GET)
    @Timed
    public List<RaspTeachers> getTeachers() {
        return scheduleService.getTeacherAll().stream().map(this::toTeacher).collect(Collectors.toList());
    }

    private RaspGroup toGroup(Group g) {
        RaspGroup rs = new RaspGroup();
        rs.setGrup(g.getName());
        return rs;
    }

    private RaspTeachers toTeacher(Teacher t) {
        RaspTeachers rs = new RaspTeachers();
        rs.setFio(t.getName());
        rs.setPrep(Long.toString(t.getId()));
        return rs;
    }

    private RaspMain toMain(Main main) {
        RaspMain rs = new RaspMain();
        rs.setAuditory(main.getAuditory().getName());
        rs.setGroupNumber(main.getGroup().getName());
        rs.setDayOfWeek(Integer.toString(main.getWeekDay()));
        rs.setPairNumber(Integer.toString(main.getLessonNumber()));
        rs.setPairType(Optional.ofNullable(main.getSubjectType()).map(SubjectType::getName).orElse(""));
        rs.setSubjectId(Long.toString(main.getSubject().getId()));
        rs.setTeacherId(Long.toString(main.getTeacher().getId()));
        rs.setWeek("1");
        return rs;
    }

    private RaspSubject toSubject(Subject s) {
        RaspSubject dto = new RaspSubject();
        dto.setDis(Long.toString(s.getId()));
        dto.setDisp(s.getName());
        dto.setDiss(s.getShortName());
        return dto;
    }

}
