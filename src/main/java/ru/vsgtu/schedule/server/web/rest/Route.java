package ru.vsgtu.schedule.server.web.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by HOME on 26.05.2016.
 */
@Controller
public class Route {
    @RequestMapping("/")
    public String index() {
        return "forward://index.xhtml";
    }

}
