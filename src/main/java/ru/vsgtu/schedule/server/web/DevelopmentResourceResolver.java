package ru.vsgtu.schedule.server.web;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.ProjectStage;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.ResourceResolver;
import java.io.File;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
//ru.vsgtu.schedule.server.web.DevelopmentResourceResolver.directories
public class DevelopmentResourceResolver extends ResourceResolver {
    public static final String PROPERTY_DIRECTORIES = DevelopmentResourceResolver.class.getName() + ".directories";


    private static final Logger logger = LoggerFactory.getLogger(DevelopmentResourceResolver.class);

    private List<File> directories;
    private final ResourceResolver defaultResolver;


    public DevelopmentResourceResolver(ResourceResolver defaultResolver) {
        this.defaultResolver = defaultResolver;

        if (FacesContext.getCurrentInstance().getApplication().getProjectStage() == ProjectStage.Development) {
            String directoryNames = System.getProperty(PROPERTY_DIRECTORIES);
            if (directoryNames == null) {
                directoryNames = System.getenv(PROPERTY_DIRECTORIES);
            }
            if (directoryNames != null) {
                directories = new LinkedList<>();

                StringTokenizer strToken = new StringTokenizer(directoryNames, System.getProperty("path.separator"));
                while (strToken.hasMoreElements()) {
                    String directoryName = strToken.nextToken();
                    File directory = new File(directoryName);
                    if (!directory.exists()) {
                        logger.warn("Specified path does not exist (still adding to directory list): {}", directoryName);
                    } else if (!directory.isDirectory()) {
                        logger.error("Specified path is not a directory (skipping): {}", directoryName);
                        continue;
                    }
                    directories.add(directory);
                }

                if (directories.isEmpty()) {
                    directories = null;
                }
            }
        }
    }

    @Override
    public URL resolveUrl(String s) {
        logger.debug("Resolving URL for resource: {}", s);
        try {
            if (directories != null) {
                for (File directory : directories) {
                    // construct a file using relative name
                    File resourceFile = new File(directory, s);
                    if (resourceFile.exists()) {
                        logger.debug("Resource resolved as: {}", resourceFile.getAbsolutePath());
                        return resourceFile.toURI().toURL();
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Failed to resolve URL for resource: {}", s, e);
        }
        // fall back to default resolver if any
        if (defaultResolver != null) {
            return defaultResolver.resolveUrl(s);
        }
        // failed to find resource and no resolver is specified
        return null;
    }
}

