package ru.vsgtu.schedule.server.web.bean;

import ru.vsgtu.schedule.server.domain.Auditory;
import ru.vsgtu.schedule.server.service.ScheduleService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.List;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
@Named("auditory")
@ViewScoped
public class AuditoryBean {
    private List<Auditory> list = Collections.emptyList();
    @Inject
    private ScheduleService scheduleService;

    @PostConstruct
    public void init() {
        list = scheduleService.getAuditoryAll();
    }

    public List<Auditory> getList() {
        return list;
    }
}
