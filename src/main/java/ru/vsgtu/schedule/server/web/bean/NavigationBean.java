package ru.vsgtu.schedule.server.web.bean;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by HOME on 22.05.2016.
 */
@Named("navigation")
@SessionScoped
public class NavigationBean implements Serializable {
    private String page = "empty";

    public void navigate() {
        FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        System.out.println(page);
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
