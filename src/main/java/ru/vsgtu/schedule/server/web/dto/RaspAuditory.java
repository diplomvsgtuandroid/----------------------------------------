
package ru.vsgtu.schedule.server.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "kor",
    "aud",
    "tip",
    "kol",
    "kaf",
    "auks"
})
public class RaspAuditory {

    @JsonProperty("kor")
    private String kor;
    @JsonProperty("aud")
    private String aud;
    @JsonProperty("tip")
    private String tip;
    @JsonProperty("kol")
    private String kol;
    @JsonProperty("kaf")
    private String kaf;
    @JsonProperty("auks")
    private String auks;


    /**
     * 
     * @return
     *     The kor
     */
    @JsonProperty("kor")
    public String getKor() {
        return kor;
    }

    /**
     * 
     * @param kor
     *     The kor
     */
    @JsonProperty("kor")
    public void setKor(String kor) {
        this.kor = kor;
    }

    /**
     * 
     * @return
     *     The aud
     */
    @JsonProperty("aud")
    public String getAud() {
        return aud;
    }

    /**
     * 
     * @param aud
     *     The aud
     */
    @JsonProperty("aud")
    public void setAud(String aud) {
        this.aud = aud;
    }

    /**
     * 
     * @return
     *     The tip
     */
    @JsonProperty("tip")
    public String getTip() {
        return tip;
    }

    /**
     * 
     * @param tip
     *     The tip
     */
    @JsonProperty("tip")
    public void setTip(String tip) {
        this.tip = tip;
    }

    /**
     * 
     * @return
     *     The kol
     */
    @JsonProperty("kol")
    public String getKol() {
        return kol;
    }

    /**
     * 
     * @param kol
     *     The kol
     */
    @JsonProperty("kol")
    public void setKol(String kol) {
        this.kol = kol;
    }

    /**
     * 
     * @return
     *     The kaf
     */
    @JsonProperty("kaf")
    public String getKaf() {
        return kaf;
    }

    /**
     * 
     * @param kaf
     *     The kaf
     */
    @JsonProperty("kaf")
    public void setKaf(String kaf) {
        this.kaf = kaf;
    }

    /**
     * 
     * @return
     *     The auks
     */
    @JsonProperty("auks")
    public String getAuks() {
        return auks;
    }

    /**
     * 
     * @param auks
     *     The auks
     */
    @JsonProperty("auks")
    public void setAuks(String auks) {
        this.auks = auks;
    }
}
