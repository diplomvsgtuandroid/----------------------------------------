package ru.vsgtu.schedule.server.web.bean;

import org.primefaces.context.RequestContext;
import ru.vsgtu.schedule.server.domain.Group;
import ru.vsgtu.schedule.server.service.ScheduleService;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HOME on 22.05.2016.
 */
@Named("groups")
@SessionScoped
public class GroupBean implements Serializable {
    @Inject
    private ScheduleService scheduleService;
    private List<Group> groups;
    private Group group;
    @PostConstruct
    public void init() {
        groups = scheduleService.getGroupAll();
    }

    public List<Group> getList() {
        return groups;
    }

    public void edit(Long id) {
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("resizable", false);
        options.put("draggable", false);
        group = scheduleService.getGroupById(id);
        RequestContext.getCurrentInstance().openDialog("page/edit_group.xhtml",options ,null);
    }

    public Group getGroup() {
        return group;
    }
    public void cancel() {
        RequestContext.getCurrentInstance().closeDialog(null);
    }
    public void save() {
        scheduleService.updateGroup(group);
        init();
        RequestContext.getCurrentInstance().closeDialog(null);
    }
}
