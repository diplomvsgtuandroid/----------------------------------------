package ru.vsgtu.schedule.server.web.bean;

import org.primefaces.context.RequestContext;
import ru.vsgtu.schedule.server.domain.Teacher;
import ru.vsgtu.schedule.server.service.ScheduleService;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HOME on 22.05.2016.
 */
@Named("teachers")
@SessionScoped
public class TeacherBean {
    private List<Teacher> list = Collections.emptyList();
    private Teacher teacher = new Teacher();

    @Inject
    private ScheduleService scheduleService;

    @PostConstruct
    public void init() {
        list = scheduleService.getTeacherAll();
    }

    public List<Teacher> getList() {
        return list;
    }

    public void addTeacher() {
        teacher = new Teacher();
        openDialog();
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void save() {
        scheduleService.updateTeacher(teacher);
        RequestContext.getCurrentInstance().closeDialog(null);
        init();
    }

    public void cancel() {
        RequestContext.getCurrentInstance().closeDialog(null);
    }

    public void edit(Long id) {
        teacher = scheduleService.getTeacherById(id);
        openDialog();

    }

    private void openDialog() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("resizable", false);
        options.put("draggable", false);
        RequestContext.getCurrentInstance().openDialog("page/edit_teacher.xhtml", options, null);
    }
}
