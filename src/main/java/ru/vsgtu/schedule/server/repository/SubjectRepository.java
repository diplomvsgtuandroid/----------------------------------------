package ru.vsgtu.schedule.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vsgtu.schedule.server.domain.Subject;

import java.util.Optional;

/**
 * Created by HOME on 02.05.2016.
 */
public interface SubjectRepository extends JpaRepository<Subject,Long> {
    Optional<Subject> findById(Long id);
}
