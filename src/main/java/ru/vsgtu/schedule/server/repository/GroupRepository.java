package ru.vsgtu.schedule.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vsgtu.schedule.server.domain.Group;

import java.util.Optional;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
public interface GroupRepository extends JpaRepository<Group, Long> {
    Optional<Group> findOneByName(String name);
}
