package ru.vsgtu.schedule.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.vsgtu.schedule.server.domain.Main;
import ru.vsgtu.schedule.server.domain.MainStatus;

import java.util.List;


/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
public interface MainRepository extends JpaRepository<Main,Long> {
    List<Main> findByStatus(MainStatus status);

    @Query("select m from Main m order by m.id asc ")
    List<Main> findAllOrderByGroupName();
}
