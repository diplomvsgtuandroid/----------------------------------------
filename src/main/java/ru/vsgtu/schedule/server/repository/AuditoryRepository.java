package ru.vsgtu.schedule.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vsgtu.schedule.server.domain.Auditory;

import java.util.Optional;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
public interface AuditoryRepository extends JpaRepository<Auditory, Long> {
    Optional<Auditory> findOneByName(String name);

    Optional<Auditory> findOneById(Long id);
}
