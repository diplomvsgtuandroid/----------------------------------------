package ru.vsgtu.schedule.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vsgtu.schedule.server.domain.ExecutionLog;

import java.util.Optional;

/**
 * Created by HOME on 24.04.2016.
 */
public interface ExecutionLogRepository extends JpaRepository<ExecutionLog, Long> {
    Optional<ExecutionLog> findOneById(Long userId);
}
