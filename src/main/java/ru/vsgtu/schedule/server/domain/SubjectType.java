package ru.vsgtu.schedule.server.domain;

/**
 * Created by HOME on 24.05.2016.
 */
public enum SubjectType {
    LECTURE("ЛК","лекция"),
    PRACTICE("ПР", "практика"),
    LABORATORY("ЛБ", "лабороторная");
    private String name;
    private String title;

    SubjectType(String name,String title) {
        this.name = name;
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }
}
