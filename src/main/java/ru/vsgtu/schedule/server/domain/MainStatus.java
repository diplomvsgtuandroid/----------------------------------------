package ru.vsgtu.schedule.server.domain;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
public enum MainStatus {
    SUCCESS,
    PARTIAL,
    ERROR
}
