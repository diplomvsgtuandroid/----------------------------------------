package ru.vsgtu.schedule.server.config;

import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.RAMDirectory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * Created by HOME on 02.05.2016.
 */
@Configuration
public class LuceneConfiguration {

    @Bean
    public IndexWriter createWriter() throws IOException {
        RAMDirectory dir = new RAMDirectory();
        return new IndexWriter(dir, new IndexWriterConfig(new RussianAnalyzer()));
    }
}
