package ru.vsgtu.schedule.server.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

/**
 * Created by HOME on 30.04.2016.
 */
@ConfigurationProperties(
        prefix = "app",
        ignoreUnknownFields = false
)
public class AppConfigurationProperties {
    private Async async = new Async().setCorePoolSize(2).setMaxPoolSize(6).setQueueCapacity(100);

    private Schedule schedule = new Schedule();

    private Logs logs = new Logs().setEnabled(false);

    private final Logging logging = new Logging();

    private Map<String, String> parameters;

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Logs getLogs() {
        return logs;
    }

    public Async getAsync() {
        return async;
    }

    private final Metrics metrics = new Metrics();

    public Schedule getSchedule() {
        return schedule;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public static class Logs {
        private boolean enabled;
        private int reportFrequency;

        public boolean isEnabled() {
            return enabled;
        }

        public Logs setEnabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public int getReportFrequency() {
            return reportFrequency;
        }

        public void setReportFrequency(int reportFrequency) {
            this.reportFrequency = reportFrequency;
        }
    }

    public static class Schedule {
        private List<String> url;
        private String cron;

        public List<String> getUrl() {
            return url;
        }

        public void setUrl(List<String> url) {
            this.url = url;
        }

        public String getCron() {
            return cron;
        }

        public void setCron(String cron) {
            this.cron = cron;
        }
    }

    public static class Async {
        private int corePoolSize;
        private int maxPoolSize;
        private int queueCapacity;

        public int getCorePoolSize() {
            return corePoolSize;
        }

        public Async setCorePoolSize(int corePoolSize) {
            this.corePoolSize = corePoolSize;
            return this;
        }

        public int getMaxPoolSize() {
            return maxPoolSize;
        }

        public Async setMaxPoolSize(int maxPoolSize) {
            this.maxPoolSize = maxPoolSize;
            return this;
        }

        public int getQueueCapacity() {
            return queueCapacity;
        }

        public Async setQueueCapacity(int queueCapacity) {
            this.queueCapacity = queueCapacity;
            return this;
        }
    }

    public Logging getLogging() {
        return logging;
    }

    public static class Logging {

        private final Logstash logstash = new Logstash();

        public Logstash getLogstash() {
            return logstash;
        }

        public static class Logstash {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 5000;

            private int queueSize = 512;

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public String getHost() {
                return host;
            }

            public void setHost(String host) {
                this.host = host;
            }

            public int getPort() {
                return port;
            }

            public void setPort(int port) {
                this.port = port;
            }

            public int getQueueSize() {
                return queueSize;
            }

            public void setQueueSize(int queueSize) {
                this.queueSize = queueSize;
            }
        }
    }

    public static class Metrics {

        private final Jmx jmx = new Jmx();

        private final Spark spark = new Spark();

        private final Graphite graphite = new Graphite();

        private final Logs logs = new Logs();

        public Jmx getJmx() {
            return jmx;
        }

        public Spark getSpark() {
            return spark;
        }

        public Graphite getGraphite() {
            return graphite;
        }

        public Logs getLogs() {
            return logs;
        }


        public static class Jmx {

            private boolean enabled = true;

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
        }

        public static class Spark {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 9999;

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public String getHost() {
                return host;
            }

            public void setHost(String host) {
                this.host = host;
            }

            public int getPort() {
                return port;
            }

            public void setPort(int port) {
                this.port = port;
            }
        }

        public static class Graphite {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 2003;

            private String prefix = "scheduleServer";

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public String getHost() {
                return host;
            }

            public void setHost(String host) {
                this.host = host;
            }

            public int getPort() {
                return port;
            }

            public void setPort(int port) {
                this.port = port;
            }

            public String getPrefix() {
                return prefix;
            }

            public void setPrefix(String prefix) {
                this.prefix = prefix;
            }
        }

        public static  class Logs {

            private boolean enabled = false;

            private long reportFrequency = 60;

            public long getReportFrequency() {
                return reportFrequency;
            }

            public void setReportFrequency(int reportFrequency) {
                this.reportFrequency = reportFrequency;
            }

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
        }
    }

}
