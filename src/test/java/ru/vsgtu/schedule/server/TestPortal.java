package ru.vsgtu.schedule.server;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by HOME on 25.04.2016.
 */
@Controller
@RequestMapping(value = "/test" )
public class TestPortal {

    @RequestMapping(value = "/{file}",
            produces = "text/html; charset=windows-1251",
            method = RequestMethod.GET)
    @ResponseBody
    public String getContentGroup(@PathVariable("file") String file ) throws IOException {
        return IOUtils.toString(this.getClass().getResourceAsStream("/site/" + file + ".html"), Charset.forName("windows-1251"));
    }
}
