package ru.vsgtu.schedule.server.service;

import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.RAMDirectory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import ru.vsgtu.schedule.server.ServerApplication;

import javax.inject.Inject;

/**
 * Created by HOME on 30.04.2016.
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
public class UpdateScheduleTaskTest {
    @Inject
    private ScheduleService scheduleService;

    private UpdateScheduleTask updateScheduleTask;

    private Document tree;
    @Before
    public void beforeTest() throws Exception {
        updateScheduleTask = new UpdateScheduleTask();
        updateScheduleTask.init();
        RAMDirectory dir = new RAMDirectory();
        IndexWriter indexWriter = new IndexWriter(dir, new IndexWriterConfig(new RussianAnalyzer()));
        ReflectionTestUtils.setField(updateScheduleTask, "indexWriter", indexWriter);
        ReflectionTestUtils.setField(updateScheduleTask, "scheduleService", scheduleService);

        tree = Jsoup.parse(this.getClass().getResourceAsStream("/site/raspisan.html"), "windows-1251","");
    }
    //@Test
    public void doIt() throws Exception {
       updateScheduleTask.doParseTreeAddToLucene(tree);
    }

    @Test
    public void testParseGroup() throws Exception {
        updateScheduleTask.doGroupSchedule(Jsoup.parse(this.getClass().getResourceAsStream("/site/1.html"), "windows-1251",""));
        updateScheduleTask.doGroupSchedule(Jsoup.parse(this.getClass().getResourceAsStream("/site/2.html"), "windows-1251",""));
        updateScheduleTask.doGroupSchedule(Jsoup.parse(this.getClass().getResourceAsStream("/site/3.html"), "windows-1251",""));
        updateScheduleTask.doGroupSchedule(Jsoup.parse(this.getClass().getResourceAsStream("/site/57.html"), "windows-1251",""));
    }

}