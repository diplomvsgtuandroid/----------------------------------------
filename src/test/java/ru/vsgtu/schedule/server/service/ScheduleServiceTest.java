package ru.vsgtu.schedule.server.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.vsgtu.schedule.server.ServerApplication;

import javax.inject.Inject;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
public class ScheduleServiceTest {
    @Inject
    private ScheduleService service;

    @Test
    public void testInvokeMethod() {
        service.getByName("test");
        service.getSubjectAll();
        service.getGroupAll();
        service.getTeacherAll();
    }
}
