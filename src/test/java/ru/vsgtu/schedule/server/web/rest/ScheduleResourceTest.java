package ru.vsgtu.schedule.server.web.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.vsgtu.schedule.server.ServerApplication;
import ru.vsgtu.schedule.server.domain.Subject;
import ru.vsgtu.schedule.server.domain.Teacher;
import ru.vsgtu.schedule.server.service.ScheduleService;

import java.util.Arrays;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by HOME on 25.04.2016.
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
@WebAppConfiguration
public class ScheduleResourceTest {
    private MockMvc restMvc;

    @Mock
    private ScheduleService scheduleService;

    private static Teacher createTeacher(Long id, String name) {
        Teacher t = new Teacher();
        t.setId(id);
        t.setName(name);
        return t;
    }

    private static Subject createSubject(Long id, String name, String shortName) {
        Subject s = new Subject();
        s.setId(id);
        s.setName(name);
        s.setShortName(shortName);
        return s;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        doReturn(Arrays.asList(createTeacher(1L, "name1"), createTeacher(2L, "name2"))).when(scheduleService).getTeacherAll();
        doReturn(Arrays.asList(createSubject(1L, "name1", "shortName1"), createSubject(2L, "name2", "shortName2"))).when(scheduleService).getSubjectAll();
        ScheduleResource resource = new ScheduleResource();
        ReflectionTestUtils.setField(resource, "scheduleService", scheduleService);
        this.restMvc = MockMvcBuilders.standaloneSetup(resource).build();
    }

    @Test
    public void getDmain() throws Exception {
        restMvc.perform(get("/content/rasp/rasp_dmain.json")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        restMvc.perform(get("/content/rasp/rasp_group.json")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getTeacher() throws Exception {
        restMvc.perform(get("/content/rasp/rasp_teachers.json")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{'prep':'1','fio':'name1'},{'prep':'2','fio':'name2'}]"));

    }

    @Test
    public void getSubject() throws Exception {
        restMvc.perform(get("/content/rasp/rasp_subjects.json")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{'dis':'1','diss':'shortName1','disp':'name1'},{'dis':'2','diss':'shortName2','disp':'name2'}]"));

    }
}