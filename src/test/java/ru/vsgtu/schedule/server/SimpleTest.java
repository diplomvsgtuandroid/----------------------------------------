package ru.vsgtu.schedule.server;

import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.RAMDirectory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by HOME on 02.05.2016.
 */
public class SimpleTest {
    public static void main(String[] args) throws Exception {

        String s = "лек.Химия НИКИФОРОВА Л.Л.   а.15-464";
        s = s.replaceAll("лек\\.", "Лекция ");
        s = s.replaceAll("а\\.", "Аудитория_");
        //System.out.println(s.indexOf());
        RAMDirectory dir = new RAMDirectory();
        IndexWriter indexWriter = new IndexWriter(dir, new IndexWriterConfig(new RussianAnalyzer()));
        Document doc = new Document();
        doc.add(new TextField("body", "Системное програмирование НИКИФОРОВА Л.Л.   а.15-464", Field.Store.YES));
        doc.add(new StoredField("id", 123213L));
        indexWriter.addDocument(doc);
        IndexSearcher indexSearcher = new IndexSearcher(DirectoryReader.open(indexWriter));
        Query query = new QueryParser("body", new RussianAnalyzer()).parse("\"Програмирование\"");
        TopDocs hits = indexSearcher.search(query, 10);
        for (ScoreDoc scoreDoc : hits.scoreDocs) {
            Document d = indexSearcher.doc(scoreDoc.doc);
            d.add(new StoredField("author", 12123L));
            System.out.println(scoreDoc.score + "    " + d.get("id"));
            //indexSearcher.getIndexReader().getTermVector(scoreDoc.doc,"body").get;
            indexWriter.updateDocument(new Term("id", doc.get("id")), doc);
        }
    }

    public static void main1(String[] args) throws IOException {
        //
        org.jsoup.nodes.Document doc = Jsoup.connect("http://portal.esstu.ru/spezialitet/raspisan.htm").get();
        AtomicInteger index = new AtomicInteger(1000);
        doc.select("body > table > tbody > tr > td > p > a").stream().map(Element::text).filter(s -> !s.isEmpty()).forEach(t -> {
            index.incrementAndGet();
            System.out.println("(" + index.incrementAndGet() + ",'" + t + "'),");
        });
    }
}
