package ru.vsgtu.schedule.server.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import ru.vsgtu.schedule.server.ServerApplication;
import ru.vsgtu.schedule.server.domain.ExecutionLog;
import ru.vsgtu.schedule.server.domain.ExecutionResult;

import javax.inject.Inject;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by HOME on 03.05.2016.
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
@Transactional
public class ExecutionLogRepositoryTest {

    @Inject
    private ExecutionLogRepository executionLogRepository;

    @Test
    public void insertRow() {
        ExecutionLog log = new ExecutionLog();
        log.setMessage("test");
        LocalDateTime t = LocalDateTime.now();
        log.setStartTime(t);
        log.setEndTime(t);
        log.setResult(ExecutionResult.ERROR);
        log = executionLogRepository.saveAndFlush(log);
        Assert.notNull(log.getId());
        log = executionLogRepository.getOne(log.getId());
        assertEquals("test", log.getMessage());
        assertEquals(ExecutionResult.ERROR, log.getResult());
        assertNotNull(log.getEndTime());
        assertNotNull(log.getStartTime());
        assertEquals(t, log.getStartTime());

    }

}