package ru.vsgtu.schedule.server.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.vsgtu.schedule.server.ServerApplication;
import ru.vsgtu.schedule.server.domain.Main;
import ru.vsgtu.schedule.server.domain.MainStatus;
import ru.vsgtu.schedule.server.domain.Group;
import ru.vsgtu.schedule.server.domain.Subject;
import ru.vsgtu.schedule.server.domain.Teacher;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
@Sql(scripts = "classpath:/ru/vsgtu/schedule/server/repository/main.sql")
@Transactional
public class MainRepositoryTest {
    @Inject
    private MainRepository mainRepository;
    @Inject
    private SubjectRepository subjectRepository;
    @Inject
    private TeacherRepository teacherRepository;
    @Inject
    private GroupRepository groupRepository;

    @Test

    public void save() {
        Subject subject = subjectRepository.getOne(2001L);
        Group group = groupRepository.getOne(1010L);
        Teacher teacher = teacherRepository.getOne(1003L);
        Main main = new Main();
        main.setTeacher(teacher);
        main.setGroup(group);
        main.setSubject(subject);
        main.setMessage("message");
        main.setStatus(MainStatus.PARTIAL);
        main = mainRepository.saveAndFlush(main);
        main = mainRepository.getOne(main.getId());
        assertNotNull(main);
        assertEquals(MainStatus.PARTIAL, main.getStatus());
        assertEquals(subject.getId(), main.getSubject().getId());
        assertEquals(group.getId(), main.getGroup().getId());
        assertEquals(teacher.getId(), main.getTeacher().getId());
    }

    @Test
    public void getAllMethrod() {
        List<Main> list = mainRepository.findAll();
        assertEquals(3, list.size());
    }

    @Test
    public void getBySuccess() {
         List<Main> list = mainRepository.findByStatus(MainStatus.PARTIAL);
         assertTrue(list.size() > 0);
    }
}
