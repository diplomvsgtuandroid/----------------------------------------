package ru.vsgtu.schedule.server.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.vsgtu.schedule.server.ServerApplication;
import ru.vsgtu.schedule.server.domain.Group;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
@Transactional
public class GroupRepositoryTest {
    @Inject
    private GroupRepository groupRepository;

    @Test
    public void newGroup() {
        Group group = new Group();
        group.setName("test");
        groupRepository.saveAndFlush(group);
        group = groupRepository.getOne(group.getId());
        assertNotNull(group);
        assertEquals("test", group.getName());
    }

    @Test
    public void getByname() {
        Group group = new Group();
        group.setName("test");
        groupRepository.saveAndFlush(group);
        Optional<Group> g = groupRepository.findOneByName("test");
        assertTrue(g.isPresent());
        assertEquals("test", g.get().getName());
    }

    @Test
    public void getAll() {
        List<Group> groups = groupRepository.findAll();
        assertTrue(groups.size() > 0);
    }
}
