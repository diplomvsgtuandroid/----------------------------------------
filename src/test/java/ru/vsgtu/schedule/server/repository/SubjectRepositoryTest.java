package ru.vsgtu.schedule.server.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.vsgtu.schedule.server.ServerApplication;
import ru.vsgtu.schedule.server.domain.Subject;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author Egor Savinkhin <esavinkhin@amt.ru>
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
@Transactional
public class SubjectRepositoryTest {
    @Inject
    private SubjectRepository subjectRepository;

    @Test
    public void getAll() {
        List<Subject> list = subjectRepository.findAll();
        assertTrue(list.size() > 0);
    }
}
