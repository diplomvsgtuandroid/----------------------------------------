package ru.vsgtu.schedule.server;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import ru.vsgtu.schedule.server.config.AppConfigurationProperties;
import ru.vsgtu.schedule.server.domain.ExecutionResult;
import ru.vsgtu.schedule.server.service.ExecutionLogService;
import ru.vsgtu.schedule.server.service.ScheduleService;
import ru.vsgtu.schedule.server.service.UpdateScheduleTask;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by HOME on 25.04.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
@WebIntegrationTest("server.port:0")
@ActiveProfiles(value = "test")
public class IntegrationTest {
    @Inject
    private ExecutionLogService executionLogService;

    @Inject
    private ScheduleService scheduleService;
    @Inject
    private UpdateScheduleTask task;
    @Value("${local.server.port}")
    private int port;


    private String getBaseUrl() {
        return "http://localhost:" + port+"/test/raspisan.html";
    }

    @Before
    public void before() {
        AppConfigurationProperties app = new AppConfigurationProperties();
        app.getSchedule().setUrl(Collections.singletonList(getBaseUrl()));
        ReflectionTestUtils.setField(task, "appConfigurationProperties", app);
    }

    @Test
    public void test1() {
        task.update();
        Assert.assertEquals(1, executionLogService.getAll().size());
        Assert.assertEquals(ExecutionResult.SUCCESS,executionLogService.getAll().stream().findFirst().get().getResult());
        Assert.assertTrue(scheduleService.getMainAllSuccess().size() > 0);
        //scheduleService
    }
}
